import React,{Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native'

export default class Header extends Component {
    render() {
      return (
        <View style={style.headerContainer}>
          <Image style={style.Image} resizeMode="cover" source={require("../assets/logo.jpg")}></Image>
          <Text style={style.title}>Hello world</Text>
        </View>
      );
    }
  }

  const style=StyleSheet.create({
    headerContainer :{
      height :60,
      flexDirection:"row",
      alignItems: "center",
      backgroundColor :"gray"
    },
    Image:{height:40, width:95},
    title:{marginLeft : 25, color: "white"}
  })