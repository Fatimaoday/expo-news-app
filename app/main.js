import React from 'react';
import { Text, View } from 'react-native';
import Header from '../components/header'
import SearchBox from '../components/searchBox'
import NewsList from '../components/newsList'
import {Constants } from 'expo';


export default class Main extends React.Component {
  constructor(props){
    super(props)
    this.state={
      news: []
    }
  }

  componentDidMount(){
    this.search("iraq")
  }

  search (q){
    fetch(`https://newsapi.org/v2/everything?q=${q}&apiKey=978d6c3818ff431b8c210ae86550fb1f`)
    .then(res => res.json())
    .then(news => {
      this.setState({news})
    }).catch((err)=>{
      console.log(err)
    })
  }

  updateParent(data){
    this.setState(data)
  }

  render() {
    return (
      <View style={{flex :1, marginTop: Constants.statusBarHeight}}>
          <Header />
          <SearchBox searchFun={this.search.bind(this)}/>
          <NewsList news={this.state.news}/>
      </View>
    );
  }
}